"dialog-info.png", "dialog-warning.png" and "dialog-error.png" are a modified version of [this icon](https://uxwing.com/exclamation-warning-round-black-icon/).

"icon.ico" was made by a friend of mine that did not want credit (all credit still goes to them).
