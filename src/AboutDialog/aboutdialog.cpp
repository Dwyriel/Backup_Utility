#include "cmake_config.h"
#include "aboutdialog.h"
#include "ui_aboutdialog.h"

AboutDialog::AboutDialog(QWidget *parent) : QDialog(parent), ui(new Ui::AboutDialog) {
    ui->setupUi(this);
    auto icon = QIcon(Utility::resource_IconApp);
    ui->labelIcon->setPixmap(icon.pixmap(ui->labelIcon->size()));
    ui->labelIcon->setScaledContents(true);
    QString about = "Backup Utility"
                    "\n\nEasily backup and restore files"
                    "\n\nVersion: "
                    PROJECT_VER
                    "\n\n[Source code](https://gitlab.com/Dwyriel/Backup_Utility) - ["
                    PROJECT_ORGANIZATION
                    "](https://gitlab.com/Dwyriel)"
                    "\n\nLicensed under MIT License";
    ui->labelAbout->setText(about);
    connect(ui->labelAbout, &QLabel::linkActivated, this, &AboutDialog::labelLinkclicked);
    Utility::centralizeChildWindow(parentWidget(), this);
}

AboutDialog::~AboutDialog() {
    delete ui;
}

void AboutDialog::labelLinkclicked(const QString &link) {
    QDesktopServices::openUrl(QUrl(link));
}
