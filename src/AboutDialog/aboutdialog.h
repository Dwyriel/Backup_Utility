#ifndef ABOUTDIALOG_H
#define ABOUTDIALOG_H

#include <QDialog>
#include <QIcon>
#include <QDesktopServices>

#include <src/utility.h>

namespace Ui {
    class AboutDialog;
}

class AboutDialog : public QDialog {
Q_OBJECT

public:
    explicit AboutDialog(QWidget *parent = nullptr);

    ~AboutDialog() override;

private:
    Ui::AboutDialog *ui;

private slots:

    void labelLinkclicked(const QString &link);
};

#endif // ABOUTDIALOG_H
