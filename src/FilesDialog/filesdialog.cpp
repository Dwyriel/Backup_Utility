#include "filesdialog.h"
#include "./ui_filesdialog.h"

const int userData = 0;
const int textType = 0;
const int textPath = 1;

FilesDialog::FilesDialog(QWidget *parent) : QDialog(parent), ui(new Ui::FilesDialog), actionCopyPath(new QAction(tr("Copy path"), this)), actionOpenInFileManager(new QAction(tr("Show in file manager"), this)), actionRemoveItem(new QAction(tr("Remove"), this)), anchorManager(new Utility::AnchorManager) {
    ui->setupUi(this);
    setAnchors();
    setItemList();
    if (ConfigManager::presetsAndConfig.KeepWindowGeometry)
        restoreGeometry(Utility::loadWindowGeometry(objectName() + "_" + ConfigManager::CurrentPreset().PresetName));
    else
        Utility::centralizeChildWindow(parentWidget(), this);
    connectSignals();
}

FilesDialog::~FilesDialog() {
    if (ConfigManager::presetsAndConfig.KeepWindowGeometry)
        Utility::saveWindowGeometry(objectName() + "_" + ConfigManager::CurrentPreset().PresetName, saveGeometry());
    delete actionCopyPath;
    delete actionOpenInFileManager;
    delete actionRemoveItem;
    delete anchorManager;
    delete ui;
}

void FilesDialog::resizeEvent(QResizeEvent *event) {
    QDialog::resizeEvent(event);
    anchorManager->WindowResized();
}

void FilesDialog::setAnchors() {
    anchorManager->AddAnchorItems(
            Utility::AnchorItem(ui->listItems, this, Utility::AnchorItem::AnchorPosition::TopLeft, Utility::AnchorItem::AnchorGrow::XY),
            Utility::AnchorItem(ui->btnAddFile, this, Utility::AnchorItem::AnchorPosition::TopRight, Utility::AnchorItem::AnchorGrow::None),
            Utility::AnchorItem(ui->btnAddFolder, this, Utility::AnchorItem::AnchorPosition::TopRight, Utility::AnchorItem::AnchorGrow::None),
            Utility::AnchorItem(ui->btnRemove, this, Utility::AnchorItem::AnchorPosition::TopRight, Utility::AnchorItem::AnchorGrow::None),
            Utility::AnchorItem(ui->btnClear, this, Utility::AnchorItem::AnchorPosition::TopRight, Utility::AnchorItem::AnchorGrow::None),
            Utility::AnchorItem(ui->btnClose, this, Utility::AnchorItem::AnchorPosition::BottomRight, Utility::AnchorItem::AnchorGrow::None)
    );
}

void FilesDialog::setItemList() {
    ui->listItems->clear();
    for (int i = 0; i < ConfigManager::CurrentPreset().ItemsToSave.size(); i++) {
        auto *item = new QTreeWidgetItem(ui->listItems);
        item->setData(userData, Qt::UserRole, i);
        item->setText(textType, ConfigManager::CurrentPreset().ItemsToSave[i].type == ItemType::File ? "File" : "Folder");
        item->setText(textPath, ConfigManager::CurrentPreset().ItemsToSave[i].path);
        ui->listItems->addTopLevelItem(item);
    }
    for (int i = 0; i < ui->listItems->columnCount(); i++)
        ui->listItems->resizeColumnToContents(i);
}

void FilesDialog::connectSignals() {
    connect(ui->btnAddFile, &QPushButton::clicked, this, &FilesDialog::btnAddFileClicked);
    connect(ui->btnAddFolder, &QPushButton::clicked, this, &FilesDialog::btnAddFolderClicked);
    connect(ui->btnClear, &QPushButton::clicked, this, &FilesDialog::btnClearClicked);
    connect(ui->btnClose, &QPushButton::clicked, this, &FilesDialog::btnCloseClicked);
    connect(ui->btnRemove, &QPushButton::clicked, this, &FilesDialog::btnRemoveClicked);
    connect(ui->listItems, &QTreeWidget::customContextMenuRequested, this, &FilesDialog::listItemsContextMenu);
}

void FilesDialog::btnAddFileClicked() {
    QFileDialog qFileDialog(this);
    qFileDialog.setWindowTitle(tr("Select Files"));
    qFileDialog.setDirectory(QDir::homePath());
    Utility::SetMultiFileDialog(qFileDialog);
    if (qFileDialog.exec() != QDialog::Accepted)
        return;
    auto files = qFileDialog.selectedFiles();
    for (const auto &file: files)
        if (!ConfigManager::CurrentPreset().ItemsToSave.contains(file))
            ConfigManager::CurrentPreset().ItemsToSave.push_back({file, ItemType::File});
    setItemList();
}

void FilesDialog::btnAddFolderClicked() {
    QFileDialog qFileDialog(this);
    qFileDialog.setWindowTitle(tr("Select Folder"));
    qFileDialog.setDirectory(QDir::homePath());
    Utility::SetSingleDirectoryDialog(qFileDialog);
    if (qFileDialog.exec() != QDialog::Accepted)
        return;
    auto folders = qFileDialog.selectedFiles();
    for (const auto &folder: folders)
        if (!ConfigManager::CurrentPreset().ItemsToSave.contains(folder))
            ConfigManager::CurrentPreset().ItemsToSave.push_back({folder, ItemType::Folder});
    setItemList();
}

void FilesDialog::btnClearClicked() {
    if (ui->listItems->topLevelItemCount() < 1)
        return;
    auto result = Utility::showWarningWithButtons(nullptr, QObject::tr("Confirm"), QObject::tr("Are you sure you want to delete all items from this list?"));
    if (result != QMessageBox::Yes)
        return;
    ConfigManager::CurrentPreset().ItemsToSave.clear();
    setItemList();
}

void FilesDialog::btnRemoveClicked() {
    auto itemList = ui->listItems->selectedItems();
    if (itemList.isEmpty())
        return;
    ConfigManager::CurrentPreset().ItemsToSave.removeAt(itemList.first()->data(0, Qt::UserRole).toInt());
    setItemList();
}

void FilesDialog::btnCloseClicked() {
    done(QDialog::Accepted);
}

void FilesDialog::listItemsContextMenu(const QPoint &position) {
    if (!ui->listItems->indexAt(position).isValid())
        return;
    auto item = ui->listItems->itemAt(position);
    auto connectionHandlerCopyPath = connect(actionCopyPath, &QAction::triggered, [item] { QGuiApplication::clipboard()->setText(item->text(textPath)); });
    auto connectionHandlerOpenInFileManager = connect(actionOpenInFileManager, &QAction::triggered, [this, item]{
        if (!Utility::OpenInFileBrowser(item->text(textPath)))
            Utility::showError(this, tr("Error"), tr("Couldn't open in default file manager."));
    });
    auto connectionHandlerRemoveItem = connect(actionRemoveItem, &QAction::triggered, this, &FilesDialog::btnRemoveClicked);
    QMenu menu(this);
    menu.addActions({actionCopyPath, actionOpenInFileManager, actionRemoveItem});
    menu.exec(ui->listItems->mapToGlobal(position));
    disconnect(connectionHandlerCopyPath);
    disconnect(connectionHandlerOpenInFileManager);
    disconnect(connectionHandlerRemoveItem);
}
