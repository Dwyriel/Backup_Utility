#ifndef FILESDIALOG_H
#define FILESDIALOG_H

#include <QDialog>
#include <QFileDialog>
#include <QMenu>
#include <QClipboard>

#include <src/presetandconfig.h>
#include <src/utility.h>

namespace Ui {
    class FilesDialog;
}

class FilesDialog : public QDialog {
    Q_OBJECT

public:
    explicit FilesDialog(QWidget *parent = nullptr);

    ~FilesDialog() override;

protected:
    void resizeEvent(QResizeEvent *event) override;

private:
    Ui::FilesDialog *ui;
    QAction *actionCopyPath;
    QAction *actionOpenInFileManager;
    QAction *actionRemoveItem;
    Utility::AnchorManager *anchorManager;

    void setAnchors();

    void setItemList();

    void connectSignals();

private slots:
    void btnAddFileClicked();

    void btnAddFolderClicked();

    void btnClearClicked();

    void btnRemoveClicked();

    void btnCloseClicked();

    void listItemsContextMenu(const QPoint &position);
};

#endif // FILESDIALOG_H
