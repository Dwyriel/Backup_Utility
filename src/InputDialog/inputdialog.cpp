#include "inputdialog.h"
#include "./ui_inputdialog.h"

InputDialog::InputDialog(QWidget *parent) : QDialog(parent), ui(new Ui::InputDialog), anchorManager(new Utility::AnchorManager) {
    ui->setupUi(this);
    setAnchors();
    ui->btnOk->setDisabled(true);
    connectSignals();
}

InputDialog::InputDialog(const QString &body, QWidget *parent) : QDialog(parent), ui(new Ui::InputDialog), anchorManager(new Utility::AnchorManager()) {
    ui->setupUi(this);
    setAnchors();
    ui->btnOk->setDisabled(true);
    ui->lblBody->setText(body);
    connectSignals();
}

InputDialog::InputDialog(const QString &title, const QString &body, QWidget *parent) : QDialog(parent), ui(new Ui::InputDialog), anchorManager(new Utility::AnchorManager()) {
    ui->setupUi(this);
    setAnchors();
    ui->btnOk->setDisabled(true);
    setWindowTitle(title);
    ui->lblBody->setText(body);
    connectSignals();
}

InputDialog::~InputDialog() {
    delete anchorManager;
    delete ui;
}

void InputDialog::resizeEvent(QResizeEvent *event) {
    QDialog::resizeEvent(event);
    anchorManager->WindowResized();
}

void InputDialog::setAnchors() {
    anchorManager->AddAnchorItems(
        Utility::AnchorItem(ui->lblBody, this, Utility::AnchorItem::AnchorPosition::Center, Utility::AnchorItem::AnchorGrow::None),
        Utility::AnchorItem(ui->inputText, this, Utility::AnchorItem::AnchorPosition::Center, Utility::AnchorItem::AnchorGrow::X),
        Utility::AnchorItem(ui->btnOk, this, Utility::AnchorItem::AnchorPosition::Center, Utility::AnchorItem::AnchorGrow::None),
        Utility::AnchorItem(ui->btnCancel, this, Utility::AnchorItem::AnchorPosition::Center, Utility::AnchorItem::AnchorGrow::None)
    );
}

void InputDialog::connectSignals() {
    connect(ui->btnOk, &QPushButton::clicked, this, &InputDialog::btnOkClicked);
    connect(ui->btnCancel, &QPushButton::clicked, this, &InputDialog::btnCancelClicked);
    connect(ui->inputText, &QLineEdit::textEdited, this, &InputDialog::inputTextEdited);
}

void InputDialog::btnOkClicked() {
    done(QDialog::Accepted);
}

void InputDialog::btnCancelClicked() {
    done(QDialog::Rejected);
}

void InputDialog::inputTextEdited(const QString &text) {
    ui->btnOk->setDisabled(text.isEmpty());
    OutputString = text;
}
