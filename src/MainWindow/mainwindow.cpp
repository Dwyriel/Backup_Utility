#include "mainwindow.h"
#include "./ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow), threadPool(new QThreadPool(this)), labelTimer(new QTimer(this)), anchorManager(new Utility::AnchorManager) {
    ConfigManager::Initialize();
    prepareLocalInstances();
    setWindowIcon(QIcon(Utility::resource_IconApp));
    setUiWidgetValues();
    connectSignals();
    setStartupText();
}

MainWindow::~MainWindow() {
    if (ConfigManager::presetsAndConfig.KeepWindowGeometry)
        Utility::saveWindowGeometry(this);
    threadPool->start(new SaveConfigToFileTask(ui->comboBoxPresets->currentIndex()));
    threadPool->waitForDone();
    delete anchorManager;
    delete labelTimer;
    delete threadPool;
    delete ui;
}

void MainWindow::resizeEvent(QResizeEvent *event) {
    QMainWindow::resizeEvent(event);
    anchorManager->WindowResized();
}

void MainWindow::prepareLocalInstances() {
    ui->setupUi(this);
    threadPool->setMaxThreadCount(1);
    labelTimer->setSingleShot(true);
    labelTimer->setInterval(5000);
    setUiWidgetStartValues();
}

void MainWindow::setUiWidgetStartValues() {
    if (!ui->menubar->isNativeMenuBar())
        setMinimumHeight(this->minimumHeight() + this->menuBar()->height());
    SetAnchors();
    if (ConfigManager::presetsAndConfig.KeepWindowGeometry)
        restoreGeometry(Utility::loadWindowGeometry(this));
}

void MainWindow::SetAnchors() {
    anchorManager->AddAnchorItems(
        Utility::AnchorItem(ui->comboBoxPresets, this, Utility::AnchorItem::AnchorPosition::TopLeft, Utility::AnchorItem::AnchorGrow::X),
        Utility::AnchorItem(ui->btnNewPreset, this, Utility::AnchorItem::AnchorPosition::TopRight, Utility::AnchorItem::AnchorGrow::None),
        Utility::AnchorItem(ui->btnDeletePreset, this, Utility::AnchorItem::AnchorPosition::TopRight, Utility::AnchorItem::AnchorGrow::None),
        Utility::AnchorItem(ui->inputBackupFolder, this, Utility::AnchorItem::AnchorPosition::TopLeft, Utility::AnchorItem::AnchorGrow::X),
        Utility::AnchorItem(ui->btnSearch, this, Utility::AnchorItem::AnchorPosition::TopRight, Utility::AnchorItem::AnchorGrow::None),
        Utility::AnchorItem(ui->btnRestore, this, Utility::AnchorItem::AnchorPosition::BottomLeft, Utility::AnchorItem::AnchorGrow::None),
        Utility::AnchorItem(ui->btnBackup, this, Utility::AnchorItem::AnchorPosition::BottomLeft, Utility::AnchorItem::AnchorGrow::None),
        Utility::AnchorItem(ui->lblStatus, this, Utility::AnchorItem::AnchorPosition::BottomLeft, Utility::AnchorItem::AnchorGrow::X),
        Utility::AnchorItem(ui->btnFiles, this, Utility::AnchorItem::AnchorPosition::BottomRight, Utility::AnchorItem::AnchorGrow::None),
        Utility::AnchorItem(ui->checkBoxAllPresets, this, Utility::AnchorItem::AnchorPosition::BottomLeft, Utility::AnchorItem::AnchorGrow::None)
    );
}

void MainWindow::setUiWidgetValues() {
    ui->actionMultithreaded->setChecked(ConfigManager::presetsAndConfig.Multithreaded);
    ui->actionKeepWindowGeometry->setChecked(ConfigManager::presetsAndConfig.KeepWindowGeometry);
    ui->actionWeakBackup->setChecked(ConfigManager::presetsAndConfig.WeakBackup);
    ui->actionWeakRestore->setChecked(ConfigManager::presetsAndConfig.WeakRestore);
    ui->inputBackupFolder->setText(ConfigManager::presetsAndConfig.BackupFolderPath);
    ui->checkBoxAllPresets->setChecked(ConfigManager::presetsAndConfig.BackupAllPresets);
    ui->comboBoxPresets->clear();
    for (const auto &item: ConfigManager::presetsAndConfig.Presets)
        ui->comboBoxPresets->addItem(" " + item.PresetName);
    ui->comboBoxPresets->setCurrentIndex(ui->comboBoxPresets->count() > 0 ? static_cast<int>(ConfigManager::presetsAndConfig.CurrentPresetIndex) : -1);
    setWidgetEnabled();
}

void MainWindow::setWidgetEnabled() {
    bool noPresets = ui->comboBoxPresets->count() < 1;
    ui->actionQuit->setDisabled(isBackupInProgress);
    ui->actionMultithreaded->setDisabled(isBackupInProgress);
    ui->actionKeepWindowGeometry->setDisabled(isBackupInProgress);
    ui->actionWeakBackup->setDisabled(isBackupInProgress);
    ui->actionWeakRestore->setDisabled(isBackupInProgress);
    ui->inputBackupFolder->setDisabled(isBackupInProgress);
    ui->comboBoxPresets->setDisabled(isBackupInProgress);
    ui->btnNewPreset->setDisabled(isBackupInProgress);
    ui->btnDeletePreset->setDisabled(isBackupInProgress || noPresets);
    ui->btnSearch->setDisabled(isBackupInProgress);
    ui->btnRestore->setDisabled(isBackupInProgress || noPresets);
    ui->btnFiles->setDisabled(isBackupInProgress || noPresets);
    ui->checkBoxAllPresets->setDisabled(isBackupInProgress);
}

void MainWindow::connectSignals() {
    connect(ui->actionQuit, &QAction::triggered, &QApplication::quit);
    connect(ui->actionAbout, &QAction::triggered, this, &MainWindow::actionAboutTriggered);
    connect(ui->actionMultithreaded, &QAction::toggled, this, &MainWindow::actionMultithreadedToggled);
    connect(ui->actionKeepWindowGeometry, &QAction::toggled, this, &MainWindow::actionKeepWindowGeometryToggled);
    connect(ui->actionWeakBackup, &QAction::toggled, this, &MainWindow::actionWeakBackupToggled);
    connect(ui->actionWeakRestore, &QAction::toggled, this, &MainWindow::actionWeakRestoreToggled);
    connect(ui->inputBackupFolder, &CustomLineEdit::lostFocus, this, &MainWindow::inputBackupFolderLostFocus);
    connect(ui->btnNewPreset, &QPushButton::clicked, this, &MainWindow::btnNewPresetClicked);
    connect(ui->btnDeletePreset, &QPushButton::clicked, this, &MainWindow::btnDeletePresetClicked);
    connect(ui->btnSearch, &QPushButton::clicked, this, &MainWindow::btnSearchFolderClicked);
    connect(ui->btnFiles, &QPushButton::clicked, this, &MainWindow::btnFilesClicked);
#if QT_VERSION >= QT_VERSION_CHECK(6, 7, 0)
    connect(ui->checkBoxAllPresets, &QCheckBox::checkStateChanged, this, &MainWindow::checkBoxAllPresetsStateChanged);
#else
    connect(ui->checkBoxAllPresets, &QCheckBox::stateChanged, this, &MainWindow::checkBoxAllPresetsStateChanged);
#endif
    connect(ui->btnRestore, &QPushButton::clicked, this, &MainWindow::btnRestoreClicked);
    connect(ui->btnBackup, &QPushButton::clicked, this, &MainWindow::btnBackupClicked);
    connect(&BackupManager::Instance(), &BackupManager::backupComplete, this, &MainWindow::backupComplete);
    connect(labelTimer, &QTimer::timeout, this, &MainWindow::labelTimerTimeout);
}

void MainWindow::setStartupText() {
    ui->lblStatus->setText(ConfigManager::fileLoaded ? tr("Welcome back :)") : tr("Hello there~"));
    labelTimer->start();
}

QString MainWindow::prepareBackupErrorMessage(const QList<QPair<QString, BackupManager::BackupResult> > &results) {
    QString message;
    for (const auto &[presetName, backupResult]: results) {
        if (backupResult == BackupManager::BackupResult::Success)
            continue;
        message += presetName + " - ";
        switch (backupResult) {
            case BackupManager::BackupResult::FileToBackupDoesNotExist:
                message += tr("One or more files to be backed up does not exist, please check files or enable Weak Backup");
                break;
            case BackupManager::BackupResult::FailCreateFolder:
                message += tr("Failed to create backup folder");
                break;
            case BackupManager::BackupResult::FailToBackupFiles:
                message += tr("Failed to backup files");
                break;
            case BackupManager::BackupResult::Success:
                break; // Unreachable, leaving it here so IDEs don't complain about missing cases
        }
        message += "\n";
    }
    return message.trimmed();
}

void MainWindow::actionAboutTriggered() {
    AboutDialog aboutDialog(this);
    aboutDialog.exec();
}

void MainWindow::actionMultithreadedToggled(bool checked) {
    ConfigManager::presetsAndConfig.Multithreaded = checked;
    threadPool->start(new SaveConfigToFileTask(ui->comboBoxPresets->currentIndex()));
}

void MainWindow::actionKeepWindowGeometryToggled(bool checked) {
    ConfigManager::presetsAndConfig.KeepWindowGeometry = checked;
    threadPool->start(new SaveConfigToFileTask(ui->comboBoxPresets->currentIndex()));
}

void MainWindow::actionWeakBackupToggled(bool checked) {
    ConfigManager::presetsAndConfig.WeakBackup = checked;
    threadPool->start(new SaveConfigToFileTask(ui->comboBoxPresets->currentIndex()));
}

void MainWindow::actionWeakRestoreToggled(bool checked) {
    ConfigManager::presetsAndConfig.WeakRestore = checked;
    threadPool->start(new SaveConfigToFileTask(ui->comboBoxPresets->currentIndex()));
}

void MainWindow::inputBackupFolderLostFocus() {
    QString inputtedFolder = ui->inputBackupFolder->text().trimmed();
    if (inputtedFolder == "")
        return;
    QFileInfo fileInfo(inputtedFolder);
    if (!fileInfo.exists()) {
        Utility::showError(this, tr("Error"), tr("Inserted directory does not exists"));
        ui->inputBackupFolder->setText(ConfigManager::presetsAndConfig.BackupFolderPath);
        return;
    }
    if (!fileInfo.isDir()) {
        Utility::showError(this, tr("Error"), tr("Needs to be a directory"));
        ui->inputBackupFolder->setText(ConfigManager::presetsAndConfig.BackupFolderPath);
        return;
    }
    if (fileInfo.isRoot()) {
        Utility::showError(this, tr("Error"), tr("Backup Folder can't be the root directory") + " \"" + QDir::root().absolutePath() + "\"");
        ui->inputBackupFolder->setText(ConfigManager::presetsAndConfig.BackupFolderPath);
        return;
    }
    if (!fileInfo.isWritable()) {
#ifdef Q_OS_WIN
        Utility::showWarning(this, tr("Warning"), tr("Program does not have permission to write to that directory, run the program as administrator or pick a different directory"));
#else
        Utility::showWarning(this, tr("Warning"), tr("Program does not have permission to write to that directory, run the program as root or pick a different directory"));
#endif
        ui->inputBackupFolder->setText(ConfigManager::presetsAndConfig.BackupFolderPath);
        return;
    }
    ui->inputBackupFolder->setText(inputtedFolder);
    ConfigManager::presetsAndConfig.BackupFolderPath = inputtedFolder;
    threadPool->start(new SaveConfigToFileTask(ui->comboBoxPresets->currentIndex()));
}

void MainWindow::btnNewPresetClicked() {
    InputDialog dialog(tr("New Preset"), tr("New preset name:"), this);
    if (!dialog.exec())
        return;
    if (dialog.OutputString.isEmpty())
        return;
    if (!ConfigManager::isFileNameValid(dialog.OutputString)) {
#ifdef Q_OS_WIN
        Utility::showWarning(this, tr("Warning"), tr("Invalid name, cannot contain / \\ : ? \" < > | *"));
#else
        Utility::showWarning(this, tr("Warning"), tr("Invalid name, cannot contain /"));
#endif
        btnNewPresetClicked();
        return;
    }
    if (ConfigManager::doesPresetAlreadyExists(dialog.OutputString)) {
        Utility::showError(this, tr("Error"), tr("A preset with that name already exists"));
        return;
    }
    ConfigManager::AddNewPreset(dialog.OutputString);
    setUiWidgetValues();
    threadPool->start(new SaveConfigToFileTask(ui->comboBoxPresets->currentIndex()));
}

void MainWindow::btnDeletePresetClicked() {
    auto result = Utility::showWarningWithButtons(nullptr, QObject::tr("Confirm"), QObject::tr("Are you sure you want to delete this preset?"));
    if (result != QMessageBox::Yes)
        return;
    Utility::forgetWindowGeometry(FilesDialog().objectName() + "_" + ConfigManager::presetsAndConfig.Presets[ui->comboBoxPresets->currentIndex()].PresetName);
    ConfigManager::RemovePresetAt(ui->comboBoxPresets->currentIndex());
    setUiWidgetValues();
    threadPool->start(new SaveConfigToFileTask(ui->comboBoxPresets->currentIndex()));
}

void MainWindow::btnSearchFolderClicked() {
    QFileDialog qFileDialog(this);
    qFileDialog.setWindowTitle(MainWindow::tr("Open Directory"));
    qFileDialog.setDirectory(QDir::homePath());
    Utility::SetSingleDirectoryDialog(qFileDialog);
    if (qFileDialog.exec() != QDialog::Accepted)
        return;
    QString folder = qFileDialog.selectedFiles()[0];
    QFileInfo fileInfo(folder);
    if (!fileInfo.isDir()) { //Shouldn't be needed, still verifying to make sure
        Utility::showError(this, tr("Error"), tr("Needs to be a directory"));
        return;
    }
    if (fileInfo.isRoot()) {
        Utility::showError(this, tr("Error"), tr("Backup Folder can't be the root directory") + " \"" + QDir::root().absolutePath() + "\"");
        return;
    }
    if (!fileInfo.isWritable()) {
#ifdef Q_OS_WIN
        Utility::showWarning(this, tr("Warning"), tr("Program does not have permission to write to that directory, run the program as administrator or pick a different directory"));
#else
        Utility::showWarning(this, tr("Warning"), tr("Program does not have permission to write to that directory, run the program as root or pick a different directory"));
#endif
        return;
    }
    ui->inputBackupFolder->setText(folder);
    ConfigManager::presetsAndConfig.BackupFolderPath = folder;
    threadPool->start(new SaveConfigToFileTask(ui->comboBoxPresets->currentIndex()));
}

void MainWindow::btnFilesClicked() {
    ConfigManager::presetsAndConfig.CurrentPresetIndex = ui->comboBoxPresets->currentIndex();
    FilesDialog fileDialog(this);
    fileDialog.exec();
    threadPool->start(new SaveConfigToFileTask());
}

void MainWindow::checkBoxAllPresetsStateChanged(int state) {
    ConfigManager::presetsAndConfig.BackupAllPresets = state;
    threadPool->start(new SaveConfigToFileTask(ui->comboBoxPresets->currentIndex()));
}

void MainWindow::btnRestoreClicked() {
    ConfigManager::presetsAndConfig.CurrentPresetIndex = ui->comboBoxPresets->currentIndex();
    RestoreDialog restoreDialog(this);
    if (restoreDialog.exec()) {
        ui->lblStatus->setText("Backup restored");
        labelTimer->start();
    }
    threadPool->start(new SaveConfigToFileTask());
}

void MainWindow::btnBackupClicked() {
    if (isBackupInProgress) {
        QString notDoneYet = (btnBackupClickCounter >= 2) ? tr("Not done yet baka!") : (btnBackupClickCounter == 1) ? tr("Hold your horses") : tr("Still working on it");
        ui->lblStatus->setText(notDoneYet);
        btnBackupClickCounter++;
        return;
    }
    if (labelTimer->isActive())
        labelTimer->stop();
    if (ui->comboBoxPresets->count() < 1) {
        ui->lblStatus->setText(tr("Create a Preset first"));
        labelTimer->start();
        return;
    }
    QFileInfo backupFolder(ui->inputBackupFolder->text());
    if (!backupFolder.exists() || !backupFolder.isDir() || backupFolder.isRoot() || !backupFolder.isWritable()) {
        ui->lblStatus->setText(tr("Invalid Backup Folder"));
        labelTimer->start();
        return;
    }
    ConfigManager::presetsAndConfig.CurrentPresetIndex = ui->comboBoxPresets->currentIndex();
    if (!ConfigManager::isThereItemsToSave()) {
        ui->lblStatus->setText(tr("No items to backup"));
        labelTimer->start();
        return;
    }
    isBackupInProgress = true;
    setWidgetEnabled();
    ui->lblStatus->setText(tr("Working on it.."));
    backupThread = QThread::create([this]() {
        BackupManager::Instance().Backup();
    });
    backupThread->start();
}

void MainWindow::backupComplete(QList<QPair<QString, BackupManager::BackupResult> > results) {
    bool allSuccess = std::all_of(results.begin(), results.end(), [](QPair<QString, BackupManager::BackupResult> result) { return result.second == BackupManager::BackupResult::Success; });
    bool allFail = std::none_of(results.begin(), results.end(), [](QPair<QString, BackupManager::BackupResult> result) { return result.second == BackupManager::BackupResult::Success; });
    ui->lblStatus->setText(allSuccess ? tr("Done!") : allFail ? tr("Failed :(") : tr("Partial fail"));
    if (!allSuccess)
        Utility::showError(this, tr("Error"), prepareBackupErrorMessage(results));
    isBackupInProgress = false;
    btnBackupClickCounter = 0;
    delete backupThread;
    backupThread = nullptr;
    setWidgetEnabled();
    labelTimer->start();
}

void MainWindow::labelTimerTimeout() {
    ui->lblStatus->setText("");
}
