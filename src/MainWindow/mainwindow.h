#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <QTimer>

#include <src/CustomLineEdit/customlineedit.h>
#include <src/InputDialog/inputdialog.h>
#include <src/FilesDialog/filesdialog.h>
#include <src/RestoreDialog/restoredialog.h>
#include <src/AboutDialog/aboutdialog.h>
#include <src/presetandconfig.h>
#include <src/backupmanager.h>
#include <src/utility.h>
#include <src/qrunnables.h>

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);

    ~MainWindow() override;

protected:
    void resizeEvent(QResizeEvent *event) override;

private:
    Ui::MainWindow *ui;
    QThreadPool *threadPool = nullptr;
    QTimer *labelTimer = nullptr;
    QThread *backupThread = nullptr;
    Utility::AnchorManager *anchorManager = nullptr;
    bool isBackupInProgress = false;
    int btnBackupClickCounter = 0;

    void prepareLocalInstances();

    void setUiWidgetStartValues();

    void SetAnchors();

    void setUiWidgetValues();

    void setWidgetEnabled();

    void connectSignals();

    void setStartupText();

    QString prepareBackupErrorMessage(const QList<QPair<QString, BackupManager::BackupResult> > &results);

private slots:
    void actionAboutTriggered();

    void actionMultithreadedToggled(bool checked);

    void actionKeepWindowGeometryToggled(bool checked);

    void actionWeakBackupToggled(bool checked);

    void actionWeakRestoreToggled(bool checked);

    void inputBackupFolderLostFocus();

    void btnNewPresetClicked();

    void btnDeletePresetClicked();

    void btnSearchFolderClicked();

    void btnFilesClicked();

    void checkBoxAllPresetsStateChanged(int state);

    void btnRestoreClicked();

    void btnBackupClicked();

    void backupComplete(QList<QPair<QString, BackupManager::BackupResult> > results);

    void labelTimerTimeout();
};

#endif // MAINWINDOW_H
