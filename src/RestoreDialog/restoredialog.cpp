#include "restoredialog.h"
#include "ui_restoredialog.h"

RestoreDialog::RestoreDialog(QWidget *parent) : QDialog(parent), ui(new Ui::RestoreDialog), restoreThread(nullptr), actionCopyPath(new QAction(tr("Copy folder path"), this)), actionOpenInFileManager(new QAction(tr("Show in file manager"), this)), actionRemove(new QAction(tr("Remove backup"), this)), messageBox(nullptr), anchorManager(new Utility::AnchorManager) {
    ui->setupUi(this);
    setAnchors();
    getBackups();
    setWidgetStartValues();
    connectSignals();
}

RestoreDialog::~RestoreDialog() {
    if (ConfigManager::presetsAndConfig.KeepWindowGeometry)
        Utility::saveWindowGeometry(this);
    if (restoreThread != nullptr) {
        restoreThread->wait(5000);
        if (!restoreThread->isFinished())
            restoreThread->terminate();
        delete restoreThread;
        restoreThread = nullptr;
    }
    if (messageBox != nullptr) {
        delete messageBox;
        messageBox = nullptr;
    }
    delete actionCopyPath;
    delete actionOpenInFileManager;
    delete actionRemove;
    delete anchorManager;
    delete ui;
}

void RestoreDialog::resizeEvent(QResizeEvent *event) {
    QDialog::resizeEvent(event);
    anchorManager->WindowResized();
}

void RestoreDialog::closeEvent(QCloseEvent *event) {
    if (isRestoreInProgress) {
        messageBox = Utility::createWarningWithButtons(this, "Warning", "Application is still trying to restore your backup, you may cancel it but changes made will not be reverted.\nProceed anyway?");
        auto answer = messageBox->exec();
        if (answer != QMessageBox::Yes) {
            event->ignore();
            return;
        }
        if (restoreThread != nullptr && !restoreThread->isFinished()) {
            restoreThread->terminate();
            delete restoreThread;
            restoreThread = nullptr;
        }
        if (messageBox != nullptr) {
            delete messageBox;
            messageBox = nullptr;
        }
    }
    QDialog::closeEvent(event);
}

void RestoreDialog::setAnchors() {
    anchorManager->AddAnchorItems(
        Utility::AnchorItem(ui->listBackups, this, Utility::AnchorItem::AnchorPosition::TopLeft, Utility::AnchorItem::AnchorGrow::XY),
        Utility::AnchorItem(ui->btnRestore, this, Utility::AnchorItem::AnchorPosition::BottomCenter, Utility::AnchorItem::AnchorGrow::None),
        Utility::AnchorItem(ui->btnClose, this, Utility::AnchorItem::AnchorPosition::BottomCenter, Utility::AnchorItem::AnchorGrow::None)
    );
}

void RestoreDialog::getBackups() {
    backups.clear();
    const QDir dir(ConfigManager::presetsAndConfig.BackupFolderPath + QDir::separator() + ConfigManager::CurrentPreset().PresetName);
    if (!dir.exists() || dir.isEmpty())
        return;
    for (auto &entry: dir.entryInfoList(QDir::Dirs | QDir::NoDotAndDotDot))
        if (Utility::DateTimeAsStringRegex.match(entry.fileName()).hasMatch())
            backups.emplace_back(entry);
}

void RestoreDialog::setListWidgetItems() {
    ui->listBackups->clear();
    if (backups.empty()) {
        ui->listBackups->addItem(tr("No backups found"));
        ui->listBackups->setDisabled(true);
        ui->btnRestore->setDisabled(true);
        return;
    }
    for (qsizetype i = 0; i < backups.size(); i++) {
        auto *item = new QListWidgetItem(ui->listBackups);
        item->setData(Qt::UserRole, i);
        item->setText(Utility::DateTimeFileNameToHumanReadable(backups[i].fileName()));
        ui->listBackups->addItem(item);
    }
    ui->listBackups->sortItems(Qt::DescendingOrder);
    ui->listBackups->setCurrentRow(0);
}

void RestoreDialog::setWidgetStartValues() {
    if (ConfigManager::presetsAndConfig.KeepWindowGeometry)
        restoreGeometry(Utility::loadWindowGeometry(this));
    else
        Utility::centralizeChildWindow(parentWidget(), this);
    setListWidgetItems();
}

void RestoreDialog::setWidgetEnabled() {
    ui->btnRestore->setDisabled(isRestoreInProgress);
    ui->btnClose->setDisabled(isRestoreInProgress);
    ui->listBackups->setDisabled(isRestoreInProgress);
}

void RestoreDialog::connectSignals() {
    connect(ui->btnRestore, &QPushButton::clicked, this, &RestoreDialog::btnRestoreClicked);
    connect(ui->btnClose, &QPushButton::clicked, this, &RestoreDialog::btnCloseClicked);
    connect(ui->listBackups, &QListWidget::customContextMenuRequested, this, &RestoreDialog::listBackupsContextMenu);
    connect(&BackupManager::Instance(), &BackupManager::restoreComplete, this, &RestoreDialog::restoreComplete);
}

void RestoreDialog::removeBackup(QListWidgetItem *item) {
    auto result = Utility::showWarningWithButtons(this, tr("Confirm"), tr("Are you sure you want to completely remove this backup?\nThis action cannot be undone."));
    if (result != QMessageBox::Yes)
        return;
    auto index = item->data(Qt::UserRole).toInt();
    QDir backupDir(backups.at(index).absoluteFilePath());
    if (!backupDir.removeRecursively()) {
        Utility::showError(this, tr("Error"), tr("An error occurred while removing the backup and all it's contents"));
        return;
    }
    backups.removeAt(index);
    delete item;
}

void RestoreDialog::btnRestoreClicked() {
    auto items = ui->listBackups->selectedItems();
    if (items.isEmpty())
        return;
    isRestoreInProgress = true;
    restoreThread = QThread::create([](const QString &backupPath) {
        BackupManager::Instance().Restore(backupPath);
    }, backups.at(items.first()->data(Qt::UserRole).toInt()).absoluteFilePath());
    restoreThread->start();
    setWidgetEnabled();
}

void RestoreDialog::restoreComplete(BackupManager::RestoreResult result) {
    delete restoreThread;
    restoreThread = nullptr;
    isRestoreInProgress = false;
    setWidgetEnabled();
    if (messageBox != nullptr)
        messageBox->close();
    switch (result) {
        case BackupManager::RestoreResult::Success:
            done(Accepted);
            break;
        case BackupManager::RestoreResult::FailCreateTemporaryDirectory:
            Utility::showError(nullptr, tr("Error"), tr("Couldn't create temporary folder at") + ' ' + ConfigManager::presetsAndConfig.BackupFolderPath);
            break;
        case BackupManager::RestoreResult::BackupFileDoesNotExist:
            Utility::showError(nullptr, tr("Error"), tr("One or more files to be restored does not exist\nPlease restore manually or enable Weak Restore"));
            break;
        case BackupManager::RestoreResult::FailMoveFilesToTemporaryDirectory:
            Utility::showError(nullptr, tr("Error"), tr("Couldn't backup old files before restoring"));
            break;
        case BackupManager::RestoreResult::FailToRestoreFiles:
            Utility::showError(nullptr, tr("Error"), tr("Failed to restore files\nCheck temporary folder at") + ' ' + ConfigManager::presetsAndConfig.BackupFolderPath + ' ' + tr("to manually recover any overwritten files"));
            break;
    }
}

void RestoreDialog::btnCloseClicked() {
    done(Rejected);
}

void RestoreDialog::listBackupsContextMenu(const QPoint &position) {
    if (!ui->listBackups->indexAt(position).isValid())
        return;
    auto item = ui->listBackups->itemAt(position);
    auto connectionHandlerCopyPath = connect(actionCopyPath, &QAction::triggered, [this, item] { QGuiApplication::clipboard()->setText(backups.at(item->data(Qt::UserRole).toInt()).absoluteFilePath()); });
    auto connectionHandlerOpenInFileManager = connect(actionOpenInFileManager, &QAction::triggered, [this, item] {
        if (!Utility::OpenInFileBrowser(backups.at(item->data(Qt::UserRole).toInt()).absoluteFilePath()))
            Utility::showError(this, tr("Error"), tr("Couldn't open in default file manager."));
    });
    auto connectionHandlerRemove = connect(actionRemove, &QAction::triggered, [this, item] { removeBackup(item); });
    QMenu menu;
    menu.addActions({actionCopyPath, actionOpenInFileManager, actionRemove});
    menu.exec(ui->listBackups->mapToGlobal(position));
    disconnect(connectionHandlerCopyPath);
    disconnect(connectionHandlerOpenInFileManager);
    disconnect(connectionHandlerRemove);
}
