#ifndef RESTOREDIALOG_H
#define RESTOREDIALOG_H

#include <QDialog>
#include <QtEvents>
#include <QMenu>
#include <QClipboard>
#include <QDir>
#include <QListWidgetItem>

#include "src/backupmanager.h"
#include "src/utility.h"

namespace Ui {
    class RestoreDialog;
}

class RestoreDialog : public QDialog {
    Q_OBJECT

public:
    explicit RestoreDialog(QWidget *parent = nullptr);

    ~RestoreDialog() override;

protected:
    void resizeEvent(QResizeEvent *event) override;

    void closeEvent(QCloseEvent *event) override;

private:
    Ui::RestoreDialog *ui;
    QThread *restoreThread;
    QAction *actionCopyPath;
    QAction *actionOpenInFileManager;
    QAction *actionRemove;
    QMessageBox *messageBox;
    Utility::AnchorManager *anchorManager;
    QList<QFileInfo> backups;
    bool isRestoreInProgress = false;

    void setAnchors();

    void getBackups();

    void setListWidgetItems();

    void setWidgetStartValues();

    void setWidgetEnabled();

    void connectSignals();

    void removeBackup(QListWidgetItem *item);

private slots:
    void btnRestoreClicked();

    void restoreComplete(BackupManager::RestoreResult result);

    void btnCloseClicked();

    void listBackupsContextMenu(const QPoint &position);
};

#endif // RESTOREDIALOG_H
