#include "backupmanager.h"

/*BackupManager*/
const QString BackupManager::tempBackupFolderName = ".temp_RestoreBackup";
BackupManager BackupManager::backupManagerInstance = BackupManager();

BackupManager &BackupManager::Instance() {
    return backupManagerInstance;
}

BackupManager::BackupManager(QWidget *parent) : QObject(parent) {
    threadPool = new QThreadPool();
}

BackupManager::~BackupManager() {
    delete threadPool;
}

void BackupManager::Backup() {
    if (ConfigManager::presetsAndConfig.Multithreaded)
        BackupMT(!ConfigManager::presetsAndConfig.BackupAllPresets ? ConfigManager::presetsAndConfig.CurrentPresetIndex : -1);
    else
        BackupST(!ConfigManager::presetsAndConfig.BackupAllPresets ? ConfigManager::presetsAndConfig.CurrentPresetIndex : -1);
}

void BackupManager::Restore(const QString &backupFolder) {
    QDir tempBackupFolder(ConfigManager::presetsAndConfig.BackupFolderPath + QDir::separator() + tempBackupFolderName);
    tempBackupFolder.removeRecursively();
    if (!tempBackupFolder.exists())
        if (!tempBackupFolder.mkpath(tempBackupFolder.path())) {
            emit restoreComplete(RestoreResult::FailCreateTemporaryDirectory);
            return;
        }
    QList<QPair<QFileInfo, QFileInfo> > filesToRestore;
    if (!GetFilesToRestore(ConfigManager::CurrentPreset(), backupFolder, filesToRestore)) {
        emit restoreComplete(RestoreResult::BackupFileDoesNotExist);
        return;
    }
    if (!MoveFiles(ConfigManager::CurrentPreset(), tempBackupFolder)) {
        emit restoreComplete(RestoreResult::FailMoveFilesToTemporaryDirectory);
        return;
    }
    if (!(ConfigManager::presetsAndConfig.Multithreaded ? RestoreMT(filesToRestore) : RestoreST(filesToRestore)))
        return;
    emit restoreComplete(RestoreResult::Success);
}

void BackupManager::BackupMT(const qint64 index) {
    QList<QPair<QString, BackupResult> > results;
    if (index >= 0) {
        QList<QFuture<bool> > tasks;
        results.emplace_back(QPair<QString, BackupResult>{ConfigManager::presetsAndConfig.Presets[index].PresetName, BackupPresetMT(ConfigManager::presetsAndConfig.Presets[index], tasks)});
        for (auto &task: tasks)
            if (!task.result())
                results.back().second = BackupResult::FailToBackupFiles;
        emit backupComplete(results);
        return;
    }
    QList<QList<QFuture<bool> > > taskList;
    QList<Preset *> presets;
    for (auto &preset: ConfigManager::presetsAndConfig.Presets)
        if (preset.ItemsToSave.size() > 0)
            presets.emplace_back(&preset);
    for (auto preset: presets) {
        taskList.emplace_back(QList<QFuture<bool> >());
        results.emplace_back(QPair<QString, BackupResult>{preset->PresetName, BackupPresetMT(*preset, taskList.back())});
    }
    for (qsizetype i = 0; i < taskList.size(); i++) {
        for (auto &task: taskList[i])
            if (!task.result())
                results[i].second = BackupResult::FailToBackupFiles;
    }
    emit backupComplete(results);
}

bool BackupManager::RestoreMT(const QList<QPair<QFileInfo, QFileInfo> > &filesToRestore) {
    QList<QFuture<bool> > tasks;
    bool success = true;
    for (const auto &[from, to]: filesToRestore) {
        if (from.isFile()) {
            tasks.push_back(QtConcurrent::run(threadPool, &BackupManager::BackupFile, this, QDir(to.path()), from.absoluteFilePath()));
            continue;
        }
        if (from.isDir())
            if (!BackupEverythingInDirMT(QDir(to.path()), from.absoluteFilePath(), tasks)) {
                success = false;
                break;
            }
    }
    for (auto &task: tasks)
        if (!task.result())
            success = false;
    if (!success) {
        emit restoreComplete(RestoreResult::FailToRestoreFiles);
        return false;
    }
    return true;
}

BackupManager::BackupResult BackupManager::BackupPresetMT(Preset &presetToBackup, QList<QFuture<bool> > &tasks) {
    QList<ItemToSave> itemsToSave;
    if (!GetFilesToBackup(presetToBackup, itemsToSave))
        return BackupResult::FileToBackupDoesNotExist;
    QDir presetFolder(ConfigManager::presetsAndConfig.BackupFolderPath + QDir::separator() + presetToBackup.PresetName);
    if (!presetFolder.exists())
        if (!presetFolder.mkpath(presetFolder.path()))
            return BackupResult::FailCreateFolder;
    QDir backupFolder(presetFolder.path() + QDir::separator() + Utility::CurrentDateTimeAsString());
    if (!backupFolder.exists())
        if (!backupFolder.mkpath(backupFolder.path()))
            return BackupResult::FailCreateFolder;
    for (auto &item: itemsToSave) {
        switch (item.type) {
            case ItemType::File:
                tasks.push_back(QtConcurrent::run(threadPool, &BackupManager::BackupFile, this, backupFolder, item.path));
                break;
            case ItemType::Folder:
                if (!BackupEverythingInDirMT(backupFolder, item.path, tasks))
                    return BackupResult::FailToBackupFiles;
                break;
        }
    }
    return BackupResult::Success;
}

bool BackupManager::BackupEverythingInDirMT(const QDir &backupFolder, const QString &folderToBackup, QList<QFuture<bool> > &tasks) {
    QFileInfo folderInfo(folderToBackup);
    QDir folder(folderToBackup);
    folder.setFilter(QDir::NoDotAndDotDot | QDir::AllDirs | QDir::Files);
    QFileInfo newFolderInfo(backupFolder.path() + QDir::separator() + folderInfo.fileName());
    if (newFolderInfo.exists())
        if (!RemovePathRecursively(newFolderInfo))
            return false;
    if (!backupFolder.mkpath(newFolderInfo.absoluteFilePath()))
        return false;
    QDir newFolder(newFolderInfo.absoluteFilePath());
    for (auto &entryInfo: folder.entryInfoList()) {
        if (entryInfo.isFile())
            tasks.push_back(QtConcurrent::run(threadPool, &BackupManager::BackupFile, this, newFolder, entryInfo.absoluteFilePath()));
        if (entryInfo.isDir())
            if (!BackupEverythingInDirMT(newFolder, entryInfo.absoluteFilePath(), tasks))
                return false;
    }
    return true;
}

void BackupManager::BackupST(const qint64 index) {
    QList<QPair<QString, BackupResult> > results;
    if (index >= 0) {
        results.emplace_back(QPair<QString, BackupResult>{ConfigManager::presetsAndConfig.Presets[index].PresetName, BackupPresetST(ConfigManager::presetsAndConfig.Presets[index])});
        emit backupComplete(results);
        return;
    }
    for (auto &preset: ConfigManager::presetsAndConfig.Presets)
        if (preset.ItemsToSave.size() > 0)
            results.emplace_back(QPair<QString, BackupResult>{preset.PresetName, BackupPresetST(preset)});
    emit backupComplete(results);
}

bool BackupManager::RestoreST(const QList<QPair<QFileInfo, QFileInfo> > &filesToRestore) {
    for (const auto &[from, to]: filesToRestore) {
        if (from.isFile()) {
            if (!BackupFile(QDir(to.path()), from.absoluteFilePath())) {
                emit restoreComplete(RestoreResult::FailToRestoreFiles);
                return false;
            }
            continue;
        }
        if (from.isDir()) {
            if (!BackupEverythingInDirST(QDir(to.path()), from.absoluteFilePath())) {
                emit restoreComplete(RestoreResult::FailToRestoreFiles);
                return false;
            }
        }
    }
    return true;
}

BackupManager::BackupResult BackupManager::BackupPresetST(Preset &presetToBackup) {
    QList<ItemToSave> itemsToSave;
    if (!GetFilesToBackup(presetToBackup, itemsToSave))
        return BackupResult::FileToBackupDoesNotExist;
    QDir presetFolder(ConfigManager::presetsAndConfig.BackupFolderPath + QDir::separator() + presetToBackup.PresetName);
    if (!presetFolder.exists())
        if (!presetFolder.mkpath(presetFolder.path()))
            return BackupResult::FailCreateFolder;
    QDir backupFolder(presetFolder.path() + QDir::separator() + Utility::CurrentDateTimeAsString());
    if (!backupFolder.exists())
        if (!backupFolder.mkpath(backupFolder.path()))
            return BackupResult::FailCreateFolder;
    for (auto &item: itemsToSave) {
        switch (item.type) {
            case ItemType::File:
                if (!BackupFile(backupFolder, item.path))
                    return BackupResult::FailToBackupFiles;
                break;
            case ItemType::Folder:
                if (!BackupEverythingInDirST(backupFolder, item.path))
                    return BackupResult::FailToBackupFiles;
                break;
        }
    }
    return BackupResult::Success;
}

bool BackupManager::BackupEverythingInDirST(const QDir &backupFolder, const QString &folderToBackup) {
    QFileInfo folderInfo(folderToBackup);
    QDir folder(folderToBackup);
    folder.setFilter(QDir::NoDotAndDotDot | QDir::AllDirs | QDir::Files);
    QFileInfo newFolderInfo(backupFolder.path() + QDir::separator() + folderInfo.fileName());
    if (newFolderInfo.exists())
        if (!RemovePathRecursively(newFolderInfo))
            return false;
    if (!backupFolder.mkpath(newFolderInfo.absoluteFilePath()))
        return false;
    QDir newFolder(newFolderInfo.absoluteFilePath());
    for (auto &entryInfo: folder.entryInfoList()) {
        if (entryInfo.isFile()) {
            if (!BackupFile(newFolder, entryInfo.absoluteFilePath()))
                return false;
            continue;
        }
        if (entryInfo.isDir())
            if (!BackupEverythingInDirST(newFolder, entryInfo.absoluteFilePath()))
                return false;
    }
    return true;
}

bool BackupManager::GetFilesToBackup(const Preset &presetToBackup, QList<ItemToSave> &filesToBackup) {
    for (auto &item: presetToBackup.ItemsToSave) {
        if (!QFileInfo::exists(item.path)) {
            if (ConfigManager::presetsAndConfig.WeakBackup)
                continue;
            return false;
        }
        filesToBackup.append(item);
    }
    return true;
}

bool BackupManager::BackupFile(const QDir &backupFolder, const QString &fileToBackup) {
    QFileInfo fileInfo(fileToBackup);
    QFileInfo newFileInfo(backupFolder.path() + QDir::separator() + fileInfo.fileName());
    if (newFileInfo.exists())
        QFile::remove(newFileInfo.absoluteFilePath());
    if (!QFile::copy(fileToBackup, newFileInfo.absoluteFilePath()))
        return false;
    return true;
}

bool BackupManager::RemovePathRecursively(const QFileInfo &path) {
    if (!path.isDir()) {
        if (!QFile::remove(path.path()))
            return false;
    } else {
        QDir qDir(path.path());
        if (!qDir.removeRecursively())
            return false;
    }
    return true;
}

bool BackupManager::MoveFiles(const Preset &preset, const QDir &destination) {
    for (auto &item: preset.ItemsToSave) {
        if (item.type == ItemType::File) {
            QFileInfo file(item.path);
            if (file.exists())
                if (!QFile::rename(item.path, destination.path() + QDir::separator() + file.fileName()))
                    return false;
            continue;
        }
        if (item.type == ItemType::Folder) {
            QDir dir(item.path);
            if (dir.exists())
                if (!dir.rename(dir.absolutePath(), destination.path() + QDir::separator() + dir.dirName()))
                    return false;
        }
    }
    return true;
}

bool BackupManager::GetFilesToRestore(const Preset &preset, const QDir &backupFolder, QList<QPair<QFileInfo, QFileInfo> > &filesToRestore) {
    for (const auto &item: preset.ItemsToSave) {
        QFileInfo file(item.path);
        QFileInfo backupedFile(backupFolder.path() + QDir::separator() + file.fileName());
        if (!backupedFile.exists()) {
            if (ConfigManager::presetsAndConfig.WeakRestore)
                continue;
            return false;
        }
        filesToRestore.append({backupedFile, file});
    }
    return true;
}
