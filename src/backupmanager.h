#ifndef BACKUPMANAGER_H
#define BACKUPMANAGER_H

#include <QWidget>
#include <QThreadPool>
#include <QtConcurrent>

#include <src/presetandconfig.h>
#include <src/utility.h>

class BackupManager : public QObject {
    Q_OBJECT

public:
    [[nodiscard]] static BackupManager &Instance();

    enum class BackupResult {
        Success,
        FileToBackupDoesNotExist,
        FailCreateFolder,
        FailToBackupFiles,
    };

    enum class RestoreResult : qint8 {
        Success,
        FailCreateTemporaryDirectory,
        BackupFileDoesNotExist,
        FailMoveFilesToTemporaryDirectory,
        FailToRestoreFiles,
    };

    void Backup();

    void Restore(const QString &backupFolder);

signals:
    void backupComplete(QList<QPair<QString, BackupResult> > result);

    void restoreComplete(RestoreResult result);

private:
    static const QString tempBackupFolderName;
    static BackupManager backupManagerInstance;

    QThreadPool *threadPool;

    explicit BackupManager(QWidget *parent = nullptr);

    ~BackupManager() override;

    void BackupMT(qint64 index = -1);

    bool RestoreMT(const QList<QPair<QFileInfo, QFileInfo> > &filesToRestore);

    BackupResult BackupPresetMT(Preset &presetToBackup, QList<QFuture<bool> > &tasks);

    bool BackupEverythingInDirMT(const QDir &backupFolder, const QString &folderToBackup, QList<QFuture<bool> > &tasks);

    void BackupST(qint64 index = -1);

    bool RestoreST(const QList<QPair<QFileInfo, QFileInfo> > &filesToRestore);

    BackupResult BackupPresetST(Preset &presetToBackup);

    bool BackupEverythingInDirST(const QDir &backupFolder, const QString &folderToBackup);

    bool GetFilesToBackup(const Preset &presetToBackup, QList<ItemToSave> &filesToBackup);

    bool BackupFile(const QDir &backupFolder, const QString &fileToBackup);

    bool RemovePathRecursively(const QFileInfo &path);

    bool MoveFiles(const Preset &preset, const QDir &destination);

    bool GetFilesToRestore(const Preset &preset, const QDir &backupFolder, QList<QPair<QFileInfo, QFileInfo> > &filesToRestore);
};

#endif // BACKUPMANAGER_H
