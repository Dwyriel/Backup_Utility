#include "cmake_config.h"
#include "presetandconfig.h"
#include <utility>

/*Json key names*/
const char *Version = "Version";
const char *Multithreaded = "Multithreaded";
const char *KeepWindowGeometry = "KeepWindowGeometry";
const char *WeakBackup = "WeakBackup";
const char *WeakRestore = "WeakRestore";
const char *BackupAllPresets = "BackupAllPresets";
const char *BackupFolderPath = "BackupFolderPath";
const char *CurrentPresetIndex = "CurrentPresetIndex";
const char *Presets = "Presets";
const char *PresetName = "PresetName";
const char *ItemsToSave = "ItemsToSave";
const char *ItemsToSavePath = "ItemsToSavePath";
const char *ItemsToSaveType = "ItemsToSaveType";
[[deprecated]] const char *BackupNumber = "BackupNumber"; //can be safely deleted
[[deprecated]] const char *FilesToSave = "FilesToSave";
[[deprecated]] const char *FoldersToSave = "FoldersToSave";

/*ItemToSave*/
bool ItemToSave::operator==(const ItemToSave &other) const {
    return other.path == this->path;
}

bool ItemToSave::operator==(const QString &path) const {
    return path == this->path;
}

/*Preset*/
Preset::Preset() = default;

Preset::Preset(QString presetName) : PresetName(std::move(presetName)) {}

Preset::Preset(QString presetName, QList<ItemToSave> itemsToSave) : PresetName(std::move(presetName)), ItemsToSave(std::move(itemsToSave)) {}

/*PresetsAndConfig*/
PresetsAndConfig::PresetsAndConfig() = default;

/*ConfigManager*/
PresetsAndConfig ConfigManager::defaultPresetsAndConfig = PresetsAndConfig();
QString ConfigManager::PresetsAndConfigFileName = "Backup_Utility.conf";
QFileInfo ConfigManager::PresetsAndConfigFile;
const QString ConfigManager::AppVersion = PROJECT_VER;
PresetsAndConfig ConfigManager::presetsAndConfig;
bool ConfigManager::fileLoaded = false;

void ConfigManager::errorLoadingConfig() {
    auto result = Utility::showWarningWithButtons(nullptr, QObject::tr("Error"), QObject::tr("Could not load configuration file, load default settings and continue?"));
    if (result != QMessageBox::Yes)
        exit(1);
    presetsAndConfig = defaultPresetsAndConfig;
}

bool ConfigManager::convertConfigToNewStructure(const QString &configFileVersionString, QJsonObject &jsonObject) {
    constexpr auto version = [](int a, int b, int c) { return static_cast<long long>(a) << 32 | static_cast<long long>(b) << 16 | c; };
    qint64 configFileVersion = 0;
    if (!configFileVersionString.isEmpty()) {
        auto versions = configFileVersionString.split('.');
        if (versions.size() != 3)
            return false;
        configFileVersion = version(versions[0].toInt(), versions[1].toInt(), versions[2].toInt());
    }
    if (configFileVersion < version(1, 3, 0))
        covertFromVersionOlderThan1_3_0(jsonObject);
    if (configFileVersion < version(1, 4, 2))
        covertFromVersionOlderThan1_4_2(jsonObject);
    if (configFileVersion < version(1, 5, 1))
        covertFromVersionOlderThan1_5_1(jsonObject);
    return true;
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
// ReSharper disable CppDeprecatedEntity

void ConfigManager::covertFromVersionOlderThan1_3_0(QJsonObject &jsonObject) {
    auto arrValue = jsonObject.take(Presets).toArray();
    QJsonArray arrToInsertBack;
    while (!arrValue.isEmpty()) {
        auto presetObj = arrValue.takeAt(0).toObject();
        QJsonArray itemsToSaveJsonArray;
        auto filesToSaveArr = presetObj.take(FilesToSave).toArray();
        while (!filesToSaveArr.isEmpty()) {
            QJsonObject itemsToSave;
            itemsToSave.insert(ItemsToSavePath, filesToSaveArr.takeAt(0).toString());
            itemsToSave.insert(ItemsToSaveType, static_cast<int>(ItemType::File));
            itemsToSaveJsonArray.append(itemsToSave);
        }
        auto foldersToSaveArr = presetObj.take(FoldersToSave).toArray();
        while (!foldersToSaveArr.isEmpty()) {
            QJsonObject itemsToSave;
            itemsToSave.insert(ItemsToSavePath, foldersToSaveArr.takeAt(0).toString());
            itemsToSave.insert(ItemsToSaveType, static_cast<int>(ItemType::Folder));
            itemsToSaveJsonArray.append(itemsToSave);
        }
        presetObj.insert(ItemsToSave, itemsToSaveJsonArray);
        arrToInsertBack.append(std::move(presetObj));
    }
    jsonObject.insert(Presets, std::move(arrToInsertBack));
}

void ConfigManager::covertFromVersionOlderThan1_4_2(QJsonObject &jsonObject) {
    jsonObject.insert(KeepWindowGeometry, defaultPresetsAndConfig.KeepWindowGeometry);
}

void ConfigManager::covertFromVersionOlderThan1_5_1(QJsonObject &jsonObject) {
    jsonObject.insert(WeakBackup, defaultPresetsAndConfig.WeakBackup);
    jsonObject.insert(WeakRestore, defaultPresetsAndConfig.WeakRestore);
}

// ReSharper restore CppDeprecatedEntity
#pragma GCC diagnostic pop

void ConfigManager::Initialize() {
#if DEBUG_BUILD
    PresetsAndConfigFile = QFileInfo(QCoreApplication::applicationDirPath() + QDir::separator() + ConfigManager::PresetsAndConfigFileName);
#else
    PresetsAndConfigFile = QFileInfo(QStandardPaths::writableLocation(QStandardPaths::StandardLocation::ConfigLocation) + QDir::separator() + ConfigManager::PresetsAndConfigFileName);
#endif
    if (PresetsAndConfigFile.exists())
        Load();
    else
        presetsAndConfig = defaultPresetsAndConfig;
}

Preset &ConfigManager::CurrentPreset() {
    return presetsAndConfig.Presets[presetsAndConfig.CurrentPresetIndex];
}

void ConfigManager::Load() {
    QFile file(PresetsAndConfigFile.absoluteFilePath());
    if (!file.open(QIODeviceBase::ReadOnly))
        return errorLoadingConfig();
    QByteArray bytes = file.readAll();
    file.close();
    QJsonParseError jsonError;
    QJsonDocument doc = QJsonDocument::fromJson(bytes, &jsonError);
    if (jsonError.error != QJsonParseError::NoError || !doc.isObject())
        return errorLoadingConfig();
    QJsonObject jsonObj = doc.object();
    const QString configFileVersion = jsonObj.take(Version).toString();
    if (configFileVersion != AppVersion)
        if (!convertConfigToNewStructure(configFileVersion, jsonObj))
            return errorLoadingConfig();
    presetsAndConfig.Multithreaded = jsonObj.take(Multithreaded).toBool(defaultPresetsAndConfig.Multithreaded);
    presetsAndConfig.KeepWindowGeometry = jsonObj.take(KeepWindowGeometry).toBool(defaultPresetsAndConfig.KeepWindowGeometry);
    presetsAndConfig.WeakBackup = jsonObj.take(WeakBackup).toBool(defaultPresetsAndConfig.WeakBackup);
    presetsAndConfig.WeakRestore = jsonObj.take(WeakRestore).toBool(defaultPresetsAndConfig.WeakRestore);
    presetsAndConfig.BackupAllPresets = jsonObj.take(BackupAllPresets).toBool(defaultPresetsAndConfig.BackupAllPresets);
    presetsAndConfig.BackupFolderPath = jsonObj.take(BackupFolderPath).toString();
    presetsAndConfig.CurrentPresetIndex = jsonObj.take(CurrentPresetIndex).toInt();
    auto arrValue = jsonObj.take(Presets).toArray();
    presetsAndConfig.Presets.reserve(arrValue.size());
    while (!arrValue.isEmpty()) {
        Preset preset;
        auto presetValue = arrValue.takeAt(0).toObject();
        preset.PresetName = presetValue.take(PresetName).toString();
        auto itemsToSaveJsonArr = presetValue.take(ItemsToSave).toArray();
        preset.ItemsToSave.reserve(itemsToSaveJsonArr.size());
        while (!itemsToSaveJsonArr.isEmpty()) {
            auto itemToSaveJsonObj = itemsToSaveJsonArr.takeAt(0).toObject();
            preset.ItemsToSave.push_back({itemToSaveJsonObj.take(ItemsToSavePath).toString(), static_cast<ItemType>(itemToSaveJsonObj.take(ItemsToSaveType).toInt())});
        }
        presetsAndConfig.Presets.append(std::move(preset));
    }
    CheckFilesIntegrity();
    SortPresets();
    fileLoaded = true;
}

void ConfigManager::Save(int index) {
    if (index >= 0)
        presetsAndConfig.CurrentPresetIndex = index;
    if (presetsAndConfig.CurrentPresetIndex < 0)
        presetsAndConfig.CurrentPresetIndex = 0;
    QFile file(PresetsAndConfigFile.absoluteFilePath());
    if (!file.open(QIODeviceBase::ReadWrite | QIODeviceBase::Truncate | QIODeviceBase::Text))
        return;
    QJsonObject pacJsonObj;
    pacJsonObj.insert(Version, AppVersion);
    pacJsonObj.insert(Multithreaded, presetsAndConfig.Multithreaded);
    pacJsonObj.insert(KeepWindowGeometry, presetsAndConfig.KeepWindowGeometry);
    pacJsonObj.insert(WeakBackup, presetsAndConfig.WeakBackup);
    pacJsonObj.insert(WeakRestore, presetsAndConfig.WeakRestore);
    pacJsonObj.insert(BackupAllPresets, presetsAndConfig.BackupAllPresets);
    pacJsonObj.insert(BackupFolderPath, presetsAndConfig.BackupFolderPath);
    pacJsonObj.insert(CurrentPresetIndex, presetsAndConfig.CurrentPresetIndex);
    QJsonArray presetsJsonArr;
    for (int i = 0; i < presetsAndConfig.Presets.size(); i++) {
        QJsonObject presetObj;
        presetObj.insert(PresetName, presetsAndConfig.Presets[i].PresetName);
        QJsonArray itemsToSaveJsonArr;
        for (const auto &itemToSave: presetsAndConfig.Presets[i].ItemsToSave) {
            QJsonObject itemToSaveJsonObject;
            itemToSaveJsonObject.insert(ItemsToSavePath, itemToSave.path);
            itemToSaveJsonObject.insert(ItemsToSaveType, static_cast<int>(itemToSave.type));
            itemsToSaveJsonArr.append(itemToSaveJsonObject);
        }
        presetObj.insert(ItemsToSave, itemsToSaveJsonArr);
        presetsJsonArr.insert(i, presetObj);
    }
    pacJsonObj.insert(Presets, presetsJsonArr);
    QJsonDocument doc;
    doc.setObject(pacJsonObj);
    QByteArray bytes = doc.toJson(QJsonDocument::Indented);
    QTextStream outStream(&file);
    outStream << bytes;
    file.close();
}

void ConfigManager::SortPresets() {
    std::sort(presetsAndConfig.Presets.begin(), presetsAndConfig.Presets.end(), [](const Preset &p1, const Preset &p2) {
        const QString &str1 = p1.PresetName;
        const QString &str2 = p2.PresetName;
        const auto smallestSize = std::min(str1.size(), str2.size());
        for (qsizetype i = 0; i < smallestSize; ++i) {
            if (str1[i] == str2[i])
                continue;
            QChar char1 = str1[i].toLower(), char2 = str2[i].toLower();
            if (char1 == char2)
                return str1[i] < str2[i];
            return char1 < char2;
        }
        return str1.size() < str2.size();
    });
}

void ConfigManager::CheckFilesIntegrity() {
    bool configChanged = false;
    if (!presetsAndConfig.Presets.isEmpty() && presetsAndConfig.CurrentPresetIndex >= presetsAndConfig.Presets.size()) {
        presetsAndConfig.CurrentPresetIndex = presetsAndConfig.Presets.size() - 1;
        configChanged = true;
    }
    if (presetsAndConfig.CurrentPresetIndex < 0) {
        presetsAndConfig.CurrentPresetIndex = 0;
        configChanged = true;
    }
    if (presetsAndConfig.Presets.isEmpty() && presetsAndConfig.CurrentPresetIndex != 0) {
        presetsAndConfig.CurrentPresetIndex = 0;
        configChanged = true;
    }
    if (!presetsAndConfig.BackupFolderPath.isEmpty()) {
        QFileInfo backupFolder(presetsAndConfig.BackupFolderPath);
        if (!backupFolder.exists() || !backupFolder.isDir()) {
            presetsAndConfig.BackupFolderPath = "";
            configChanged = true;
        }
    }
    QList<int> indexOfPresetsWithInvalidName;
    for (int i = 0; i < presetsAndConfig.Presets.size(); i++) {
        if (!isFileNameValid(presetsAndConfig.Presets[i].PresetName)) {
            indexOfPresetsWithInvalidName.push_back(i);
            continue;
        }
        QList<int> indexOfInvalidPaths;
        for (int j = 0; j < presetsAndConfig.Presets[i].ItemsToSave.size(); j++) {
            QFileInfo pathInfo(presetsAndConfig.Presets[i].ItemsToSave[j].path);
            if (!pathInfo.exists())
                continue;
            if (presetsAndConfig.Presets[i].ItemsToSave[j].type == ItemType::File && !pathInfo.isFile())
                indexOfInvalidPaths.push_back(j);
            if (presetsAndConfig.Presets[i].ItemsToSave[j].type == ItemType::Folder && !pathInfo.isDir())
                indexOfInvalidPaths.push_back(j);
        }
        if (!indexOfInvalidPaths.isEmpty()) {
            for (qint64 index = indexOfInvalidPaths.size() - 1; index >= 0; index--)
                presetsAndConfig.Presets[i].ItemsToSave.removeAt(indexOfInvalidPaths[index]);
            configChanged = true;
        }
    }
    if (!indexOfPresetsWithInvalidName.isEmpty()) {
        for (qint64 i = indexOfPresetsWithInvalidName.size() - 1; i >= 0; i--)
            presetsAndConfig.Presets.removeAt(indexOfPresetsWithInvalidName[i]);
        configChanged = true;
    }
    if (configChanged) {
        auto result = Utility::showWarningWithButtons(nullptr, QObject::tr("Error"), QObject::tr("Some configurations, presets or files/folders to backup are no longer valid, proceed anyway?"));
        if (result != QMessageBox::Yes)
            exit(1);
        Save();
    }
}

void ConfigManager::AddNewPreset(const QString &presetName) {
    presetsAndConfig.Presets.append(Preset(presetName));
    SortPresets();
    for (int i = 0; i < presetsAndConfig.Presets.size(); ++i)
        if (presetsAndConfig.Presets[i].PresetName == presetName) {
            presetsAndConfig.CurrentPresetIndex = i;
            break;
        }
}

void ConfigManager::RemovePresetAt(int index) {
    if (presetsAndConfig.Presets.isEmpty())
        return;
    presetsAndConfig.Presets.removeAt(index);
    presetsAndConfig.CurrentPresetIndex = (index >= presetsAndConfig.Presets.size()) ? (presetsAndConfig.Presets.size() - 1) : index;
}

bool ConfigManager::isFileNameValid(const QString &name) {
#ifdef Q_OS_WIN
    return name.contains(QRegularExpression(R"(^[^<\/*?"\\>:|]+$)"));
#else
    return !name.contains('/');
#endif
}

bool ConfigManager::isThereItemsToSave() {
    if (presetsAndConfig.Presets.isEmpty())
        return false;
    if (!presetsAndConfig.BackupAllPresets)
        return !CurrentPreset().ItemsToSave.isEmpty();
    bool filesToSave = false;
    for (auto &preset: presetsAndConfig.Presets) {
        filesToSave = !preset.ItemsToSave.isEmpty();
        if (filesToSave)
            break;
    }
    return filesToSave;
}

bool ConfigManager::doesPresetAlreadyExists(const QString &presetName) {
    for (const auto &preset: presetsAndConfig.Presets)
        if (preset.PresetName == presetName)
            return true;
    return false;
}
