#ifndef PRESETANDCONFIG_H
#define PRESETANDCONFIG_H

#include <QCoreApplication>
#include <QStandardPaths>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QtGlobal>
#include <QString>
#include <QList>
#include <QDir>
#include <QFileInfo>
#include <QStringList>

#include <src/utility.h>

enum class ItemType : char {
    File = 0,
    Folder = 1
};

struct ItemToSave {
    QString path;
    ItemType type;

    bool operator==(const ItemToSave &other) const;

    bool operator==(const QString &path) const;
};

struct Preset {
    QString PresetName;
    QList<ItemToSave> ItemsToSave;

    Preset();

    explicit Preset(QString presetName);

    explicit Preset(QString presetName, QList<ItemToSave> itemsToSave);
};

struct PresetsAndConfig {
    bool Multithreaded = true;
    bool KeepWindowGeometry = true;
    bool WeakBackup = true;
    bool WeakRestore = true;
    bool BackupAllPresets = false;
    QString BackupFolderPath;
    qint64 CurrentPresetIndex = 0;
    QList<Preset> Presets;

    PresetsAndConfig();
};

class ConfigManager {
    static PresetsAndConfig defaultPresetsAndConfig;
    static QString PresetsAndConfigFileName;
    static QFileInfo PresetsAndConfigFile;

    static void errorLoadingConfig();

    static bool convertConfigToNewStructure(const QString &configFileVersionString, QJsonObject &jsonObject);

    static void covertFromVersionOlderThan1_3_0(QJsonObject &jsonObject);

    static void covertFromVersionOlderThan1_4_2(QJsonObject &jsonObject);

    static void covertFromVersionOlderThan1_5_1(QJsonObject &jsonObject);

public:
    static const QString AppVersion;
    static PresetsAndConfig presetsAndConfig;
    static bool fileLoaded;

    ConfigManager() = delete;

    ~ConfigManager() = delete;

    static void Initialize();

    static Preset &CurrentPreset();

    static void Load();

    static void Save(int index = -1);

    static void SortPresets();

    static void CheckFilesIntegrity();

    static void AddNewPreset(const QString &presetName);

    static void RemovePresetAt(int index);

    static bool isFileNameValid(const QString &name);

    static bool isThereItemsToSave();

    static bool doesPresetAlreadyExists(const QString &presetName);
};

#endif // PRESETANDCONFIG_H
