#include "cmake_config.h"
#include "utility.h"

#define PIXMAP_SIZE 60
#define MSGBOX_STYLE_SHEET "font-size: 16px;"
#define GEOMETRY_STRING "geometry"

#ifdef Q_OS_WIN
#define DATETIME_FORMAT "yyyy.MM.dd hh.mm.ss.zzz"
#else
#define DATETIME_FORMAT "yyyy.MM.dd hh:mm:ss.zzz"
#endif

void Utility::showQMessageBox(QWidget *parent, QMessageBox::Icon type, const char *iconPath, const QString &title, const QString &body) {
    auto msgBox = QMessageBox(type, title, body, QMessageBox::Ok, parent);
    QPixmap pixmap(iconPath);
    pixmap = pixmap.scaled(PIXMAP_SIZE, PIXMAP_SIZE, Qt::KeepAspectRatio, Qt::SmoothTransformation);
    msgBox.setIconPixmap(pixmap);
    msgBox.setStyleSheet(MSGBOX_STYLE_SHEET);
    msgBox.exec();
}

int Utility::showWarningWithButtons(QWidget *parent, const QString &title, const QString &body) {
    auto msgBox = QMessageBox(QMessageBox::Icon::Warning, title, body, QMessageBox::Yes | QMessageBox::No, parent);
    QPixmap pixmap(resource_IconWarning);
    pixmap = pixmap.scaled(PIXMAP_SIZE, PIXMAP_SIZE, Qt::KeepAspectRatio, Qt::SmoothTransformation);
    msgBox.setIconPixmap(pixmap);
    msgBox.setStyleSheet(MSGBOX_STYLE_SHEET);
    return msgBox.exec();
}

QMessageBox *Utility::createWarningWithButtons(QWidget *parent, const QString &title, const QString &body) {
    auto msgBox = new QMessageBox(QMessageBox::Icon::Warning, title, body, QMessageBox::Yes | QMessageBox::No, parent);
    QPixmap pixmap(resource_IconWarning);
    pixmap = pixmap.scaled(PIXMAP_SIZE, PIXMAP_SIZE, Qt::KeepAspectRatio, Qt::SmoothTransformation);
    msgBox->setIconPixmap(pixmap);
    msgBox->setStyleSheet(MSGBOX_STYLE_SHEET);
    return msgBox;
}

QMessageBox *Utility::createInfo(QWidget *parent, const QString &title, const QString &body) {
    auto msgBox = new QMessageBox(QMessageBox::Icon::Information, title, body, QMessageBox::Ok, parent);
    QPixmap pixmap(resource_IconInfo);
    pixmap = pixmap.scaled(PIXMAP_SIZE, PIXMAP_SIZE, Qt::KeepAspectRatio, Qt::SmoothTransformation);
    msgBox->setIconPixmap(pixmap);
    msgBox->setStyleSheet(MSGBOX_STYLE_SHEET);
    return msgBox;
}

void Utility::saveWindowGeometry(const QString &group, const QByteArray &geometry) {
#if DEBUG_BUILD
    QSettings settings(QCoreApplication::applicationDirPath() + QDir::separator() + PROJECT_NAME + ".ini", QSettings::IniFormat);
#else
    QSettings settings;
#endif
    settings.beginGroup(group);
    settings.setValue(GEOMETRY_STRING, geometry);
    settings.endGroup();
}

void Utility::saveWindowGeometry(const QWidget *widget) {
    return saveWindowGeometry(widget->objectName(), widget->saveGeometry());
}

QByteArray Utility::loadWindowGeometry(const QString &group) {
#if DEBUG_BUILD
    QSettings settings(QCoreApplication::applicationDirPath() + QDir::separator() + PROJECT_NAME + ".ini", QSettings::IniFormat);
#else
    QSettings settings;
#endif
    settings.beginGroup(group);
    const auto geometry = settings.value(GEOMETRY_STRING, QByteArray()).toByteArray();
    settings.endGroup();
    return geometry;
}

QByteArray Utility::loadWindowGeometry(const QWidget *widget) {
    return loadWindowGeometry(widget->objectName());
}

void Utility::forgetWindowGeometry(const QString &group) {
#if DEBUG_BUILD
    QSettings settings(QCoreApplication::applicationDirPath() + QDir::separator() + PROJECT_NAME + ".ini", QSettings::IniFormat);
#else
    QSettings settings;
#endif
    settings.beginGroup(group);
    settings.remove("");
    settings.endGroup();
}

void Utility::forgetWindowGeometry(const QWidget *widget) {
    return forgetWindowGeometry(widget->objectName());
}

void Utility::SetMultiFileDialog(QFileDialog &qFileDialog) {
    qFileDialog.setFileMode(QFileDialog::ExistingFiles);
    qFileDialog.setAcceptMode(QFileDialog::AcceptOpen);
    qFileDialog.setOptions(QFileDialog::DontResolveSymlinks);
    qFileDialog.setViewMode(QFileDialog::Detail);
    qFileDialog.setFilter(qFileDialog.filter() | QDir::Hidden);
}

void Utility::SetSingleDirectoryDialog(QFileDialog &qFileDialog) {
    qFileDialog.setFileMode(QFileDialog::Directory);
    qFileDialog.setAcceptMode(QFileDialog::AcceptOpen);
    qFileDialog.setOptions(QFileDialog::DontResolveSymlinks | QFileDialog::ShowDirsOnly);
    qFileDialog.setViewMode(QFileDialog::List);
    qFileDialog.setFilter(qFileDialog.filter() | QDir::Hidden);
}

QString Utility::CurrentDateTimeAsString() {
    return QDateTime::currentDateTime().toString(DATETIME_FORMAT);
}

QString Utility::DateTimeFileNameToHumanReadable(const QString& datetime) {
    return QDateTime::fromString(datetime, DATETIME_FORMAT).toString("yyyy/MM/dd hh:mm:ss.zzz");
}

void Utility::centralizeChildWindow(QWidget *parent, QWidget *child) {
    if (parent == nullptr || child == nullptr)
        return;
    child->move(parent->pos().x() + ((parent->size().width() / 2) - (child->size().width() / 2)), parent->pos().y() + ((parent->size().height() / 2) - (child->size().height() / 2)));
}

bool Utility::OpenInFileBrowser(const QFileInfo &fileInfo) {
#ifdef Q_OS_WIN
    //todo | requires win enviroment to properly test
    return false;
#else
    const QString folder = fileInfo.isDir() ? fileInfo.absoluteFilePath() : fileInfo.absolutePath();
    return QProcess::startDetached("xdg-open", {folder});
#endif
}

bool Utility::OpenInFileBrowser(const QString &path) {
    return OpenInFileBrowser(QFileInfo(path));
}

Utility::AnchorItem::AnchorItem(QWidget *widget, QWidget *windowWidget, AnchorPosition position, AnchorGrow size) : widget(widget), windowWidget(windowWidget), PositionPolicy(position), SizePolicy(size) {
    if ((PositionPolicy == AnchorPosition::None || PositionPolicy == AnchorPosition::TopLeft) && SizePolicy == AnchorGrow::None)
        return;
    if (PositionPolicy != AnchorPosition::None && PositionPolicy != AnchorPosition::TopLeft)
        if (PositionPolicy == AnchorPosition::CenterLeft || PositionPolicy == AnchorPosition::BottomLeft) {
            XOffset = -1;
            YOffset = PositionPolicy == AnchorPosition::CenterLeft ? (windowWidget->height() / 2) - (widget->y() + (widget->height() / 2)) : windowWidget->height() - (widget->y() + widget->height());
        } else if (PositionPolicy == AnchorPosition::TopRight || PositionPolicy == AnchorPosition::CenterRight || PositionPolicy == AnchorPosition::BottomRight) {
            XOffset = windowWidget->width() - (widget->x() + widget->width());
            YOffset = PositionPolicy == AnchorPosition::TopRight ? -1 : PositionPolicy == AnchorPosition::CenterRight ? (windowWidget->height() / 2) - (widget->y() + (widget->height() / 2)) : windowWidget->height() - (widget->y() + widget->height());
        } else {
            XOffset = (windowWidget->width() / 2) - (widget->x() + (widget->width() / 2));
            YOffset = PositionPolicy == AnchorPosition::TopCenter ? -1 : PositionPolicy == AnchorPosition::Center ? windowWidget->height() / 2 - (widget->y() + (widget->height() / 2)) : windowWidget->height() - (widget->y() + widget->height());
        }
    if (SizePolicy != AnchorGrow::None) {
        WidthOffset = (SizePolicy == AnchorGrow::X || SizePolicy == AnchorGrow::XY) ? windowWidget->width() - widget->width() : -1;
        HeightOffset = (SizePolicy == AnchorGrow::Y || SizePolicy == AnchorGrow::XY) ? windowWidget->height() - widget->height() : -1;
    }
}

void Utility::AnchorItem::setNewPosAndSize() {
    const auto windowWidth = windowWidget->width(), windowHeight = windowWidget->height();
    auto widgetWidth = widget->width(), widgetHeight = widget->height();
    if (SizePolicy != AnchorGrow::None)
        widget->resize(static_cast<bool>(SizePolicy & AnchorGrow::X) ? windowWidth - WidthOffset : widgetWidth, static_cast<bool>(SizePolicy & AnchorGrow::Y) ? windowHeight - HeightOffset : widgetHeight);
    if (PositionPolicy == AnchorPosition::None || PositionPolicy == AnchorPosition::TopLeft)
        return;
    widgetWidth = widget->width();
    widgetHeight = widget->height();
    int newX = widget->x(), newY = widget->y();
    switch (PositionPolicy) {
        case AnchorPosition::None:
        case AnchorPosition::TopLeft:
            return;
        case AnchorPosition::CenterLeft:
            newY = windowHeight / 2 - (YOffset + (widgetHeight / 2));
            break;
        case AnchorPosition::BottomLeft:
            newY = windowHeight - (YOffset + widgetHeight);
            break;
        case AnchorPosition::TopCenter:
            newX = windowWidth / 2 - (XOffset + (widgetWidth / 2));
            break;
        case AnchorPosition::Center:
            newX = windowWidth / 2 - (XOffset + (widgetWidth / 2));
            newY = windowHeight / 2 - (YOffset + (widgetHeight / 2));
            break;
        case AnchorPosition::BottomCenter:
            newX = windowWidth / 2 - (XOffset + (widgetWidth / 2));
            newY = windowHeight - (YOffset + widgetHeight);
            break;
        case AnchorPosition::TopRight:
            newX = windowWidth - (XOffset + widgetWidth);
            break;
        case AnchorPosition::CenterRight:
            newX = windowWidth - (XOffset + widgetWidth);
            newY = windowHeight / 2 - (YOffset + (widgetHeight / 2));
            break;
        case AnchorPosition::BottomRight:
            newX = windowWidth - (XOffset + widgetWidth);
            newY = windowHeight - (YOffset + widgetHeight);
            break;
    }
    widget->move(newX, newY);
}

bool Utility::AnchorManager::isUnique(const AnchorItem &item) const {
    for (const auto &it: this->items)
        if (it.widget == item.widget)
            return false;
    return true;
}

void Utility::AnchorManager::AddAnchorItems(QList<AnchorItem> &&itemList) {
    for (auto &item: itemList)
        if (item.widget != nullptr && item.windowWidget != nullptr && isUnique(item))
            this->items.append(itemList);
}

void Utility::AnchorManager::RemoveAnchorItem(QWidget *widget) {
    for (qsizetype i = 0; i < this->items.size(); i++)
        if (this->items[i].widget == widget) {
            this->items.remove(i);
            return;
        }
}

void Utility::AnchorManager::ClearItems() {
    this->items.clear();
}

void Utility::AnchorManager::WindowResized() {
    for (auto item: this->items)
        item.setNewPosAndSize();
}
