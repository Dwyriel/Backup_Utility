#ifndef UTILITY_H
#define UTILITY_H

#include <QMessageBox>
#include <QFileDialog>
#include <QSettings>
#include <QProcess>

#include <src/presetandconfig.h>

namespace Utility {
#if Q_OS_WIN
    static const QRegularExpression DateTimeAsStringRegex(R"str(/\b[1-9][0-9]{3,}.(0[1-9]|1[0-2]).(0[1-9]|[1,2][0-9]|3[0,1]) ([0,1][0-9]|2[0-3]).[0-5][0-9].[0-5][0-9].[0-9]{3}\b/s)str");
#else
    static const QRegularExpression DateTimeAsStringRegex(R"str(\b[1-9][0-9]{3,}.(0[1-9]|1[0-2]).(0[1-9]|[1,2][0-9]|3[0,1]) ([0,1][0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9].[0-9]{3}\b)str");
#endif

    static auto resource_IconApp = ":/resources/icon/icon.ico";
    static auto resource_IconError = ":/resources/icon/dialog-error.png";
    static auto resource_IconWarning = ":/resources/icon/dialog-warning.png";
    static auto resource_IconInfo = ":/resources/icon/dialog-info.png";

    void showQMessageBox(QWidget *parent, QMessageBox::Icon type, const char *iconPath, const QString &title, const QString &body);

    inline void showError(QWidget *parent, const QString &title, const QString &body) {
        showQMessageBox(parent, QMessageBox::Icon::Critical, resource_IconError, title, body);
    }

    inline void showWarning(QWidget *parent, const QString &title, const QString &body) {
        showQMessageBox(parent, QMessageBox::Icon::Warning, resource_IconWarning, title, body);
    }

    inline void showInfo(QWidget *parent, const QString &title, const QString &body) {
        showQMessageBox(parent, QMessageBox::Icon::Information, resource_IconInfo, title, body);
    }

    int showWarningWithButtons(QWidget *parent, const QString &title, const QString &body);

    /**
     * @return Pointer to a newly created QMessageBox.
     * @note Delete pointer after use.
     */
    QMessageBox *createWarningWithButtons(QWidget *parent, const QString &title, const QString &body);

    /**
     * @return Pointer to a newly created QMessageBox.
     * @note Delete pointer after use.
     */
    QMessageBox *createInfo(QWidget *parent, const QString &title, const QString &body);

    void saveWindowGeometry(const QString &group, const QByteArray &geometry);

    void saveWindowGeometry(const QWidget *widget);

    QByteArray loadWindowGeometry(const QString &group);

    QByteArray loadWindowGeometry(const QWidget *widget);

    void forgetWindowGeometry(const QString &group);

    void forgetWindowGeometry(const QWidget *widget);

    void centralizeChildWindow(QWidget *parent, QWidget *child);

    void SetMultiFileDialog(QFileDialog &qFileDialog);

    void SetSingleDirectoryDialog(QFileDialog &qFileDialog);

    QString CurrentDateTimeAsString();

    QString DateTimeFileNameToHumanReadable(const QString &datetime);

    bool OpenInFileBrowser(const QFileInfo& fileInfo);

    bool OpenInFileBrowser(const QString& path);

    class AnchorManager;

    struct AnchorItem {
        enum class AnchorPosition : qint8 {
            None,
            TopLeft,
            TopCenter,
            TopRight,
            CenterLeft,
            Center,
            CenterRight,
            BottomLeft,
            BottomCenter,
            BottomRight
        };

        enum class AnchorGrow : qint8 {
            None = 0,
            X = 0b01,
            Y = 0b10,
            XY = 0b11,
        };

    private:
        QWidget *const widget;
        QWidget *const windowWidget;
        int XOffset = -1;
        int YOffset = -1;
        int WidthOffset = -1;
        int HeightOffset = -1;
        AnchorPosition PositionPolicy;
        AnchorGrow SizePolicy;

    public:
        AnchorItem() = delete;

        AnchorItem(QWidget *widget, QWidget *windowWidget, AnchorPosition position, AnchorGrow size);

    private:
        void setNewPosAndSize();

        friend AnchorManager;
    };

    class AnchorManager {
        QList<AnchorItem> items;

        [[nodiscard]] bool isUnique(const AnchorItem &item) const;

    public:
        void AddAnchorItems(QList<AnchorItem> &&itemList);

        template<typename... Anchor>
        void AddAnchorItems(Anchor &&... item);

        void RemoveAnchorItem(QWidget *widget);

        void ClearItems();

        void WindowResized();
    };
}

constexpr Utility::AnchorItem::AnchorGrow operator&(Utility::AnchorItem::AnchorGrow first, Utility::AnchorItem::AnchorGrow second) {
    return static_cast<Utility::AnchorItem::AnchorGrow>(static_cast<qint8>(first) & static_cast<qint8>(second));
}

constexpr Utility::AnchorItem::AnchorGrow operator|(Utility::AnchorItem::AnchorGrow first, Utility::AnchorItem::AnchorGrow second) {
    return static_cast<Utility::AnchorItem::AnchorGrow>(static_cast<qint8>(first) | static_cast<qint8>(second));
}

template<typename... Anchor>
void Utility::AnchorManager::AddAnchorItems(Anchor &&... item) {
    static_assert(std::conjunction_v<std::is_same<Anchor, AnchorItem>...>);
    QList<AnchorItem> itemList = {item...};
    AddAnchorItems(std::forward<QList<AnchorItem> >(itemList));
}

#endif // UTILITY_H
